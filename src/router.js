import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Navbar from "./components/Navbar/Navbar.vue";
import Login from "./views/Login.vue";
import Kontakt from "./views/Kontakt.vue";
import Footer from "./components/Footer/Footer.vue";
import Usluge from "./views/Usluge.vue";
import Gallery from "./views/Gallery.vue";
import DosadasnjiRadovi from "./views/DosadasnjiRadovi.vue";

Vue.use(Router);

export default new Router({
  scrollBehavior() {
    return {
      x: 0,
      y: 0
    };
  },
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      components: {
        home: Home,
        navbar: Navbar,
        footer: Footer
      }
    },
    // {
    //   path: "/about",
    //   name: "about",
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () =>
    //     import( /* webpackChunkName: "about" */ "./views/About.vue")
    // }
    {
      path: "/kontakt",
      name: "kontakt",
      components: {
        default: Kontakt,
        navbar: Navbar,
        footer: Footer
      }
    },
    {
      path: "/usluge",
      name: "usluge",
      components: {
        default: Usluge,
        navbar: Navbar,
        footer: Footer
      }
    },
    {
      path: "/gallery",
      name: "gallery",
      components: {
        default: Gallery,
        navbar: Navbar,
        footer: Footer
      }
    },
    {
      path: "/dosadasnjiradovi",
      name: "dosadasnjiradovi",
      components: {
        default: DosadasnjiRadovi,
        navbar: Navbar
      }
    },
    {
      path: "/login",
      name: "login",
      components: {
        navbar: Login
      }
    }
  ]
});
