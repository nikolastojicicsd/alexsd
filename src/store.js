import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    dark: false,
    logged: false,
    arrOfCards: [{
      naziv: "Prevoz rasutog materijala",
      lokacija: "Smederevo",
      datum: "31.01.2019.",
      cena: "15,000.00",
      opis: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In est erat, tristique non orci sit amet, faucibus vulputate odio. Curabitur ex ex, tincidunt et tempor sed.",
      slika: "thumbs/01.jpg"
    }]
  },
  mutations: {
    toggleDark(state) {
      state.dark = !state.dark;
    },
    login(state) {
      state.logged = true;
    }
  },
  actions: {},
  getters: {
    dark(state) {
      return state.dark;
    },
    logged(state) {
      return state.logged;
    }
  }
});